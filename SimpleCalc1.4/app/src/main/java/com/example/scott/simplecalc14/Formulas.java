package com.example.scott.simplecalc14;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Formulas extends AppCompatActivity implements View.OnClickListener {

    private Button btnSquare, btnCube, btnRaiseTo, btnSquareRoot, btnMax, btnMin, btnSin, btnTan, btnRound, btnHyp, btnFloor, btnCos, btnRadians, btnDegrees, btnCalculator;
    private EditText firstNum, secondNum;
    private TextView tvResultF;
    private double firNum, secNum, result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulas);

        init();
    }

    private void init(){
        initialize();
        turnOnClickListener();
    }

    private void turnOnClickListener() {
        btnSquare.setOnClickListener(this);
        btnCube.setOnClickListener(this);
        btnRaiseTo.setOnClickListener(this);
        btnSquareRoot.setOnClickListener(this);
        btnMax.setOnClickListener(this);
        btnTan.setOnClickListener(this);
        btnRound.setOnClickListener(this);
        btnHyp.setOnClickListener(this);
        btnSin.setOnClickListener(this);
        btnMin.setOnClickListener(this);
        btnFloor.setOnClickListener(this);
        btnCos.setOnClickListener(this);
        btnRadians.setOnClickListener(this);
        btnDegrees.setOnClickListener(this);
        btnCalculator.setOnClickListener(this);
    }

    private void initialize() {
        btnSquare = (Button)findViewById(R.id.btnSquare);
        btnCube = (Button)findViewById(R.id.btnCube);
        btnRaiseTo = (Button)findViewById(R.id.btnRaiseTo);
        btnSquareRoot = (Button)findViewById(R.id.btnSquareRoot);
        btnMax = (Button)findViewById(R.id.btnMax);
        btnMin = (Button)findViewById(R.id.btnMin);
        btnSin = (Button)findViewById(R.id.btnSin);
        btnTan = (Button)findViewById(R.id.btnTan);
        btnRound = (Button)findViewById(R.id.btnRound);
        btnHyp = (Button)findViewById(R.id.btnHyp);
        btnFloor = (Button)findViewById(R.id.btnFloor);
        btnCos = (Button)findViewById(R.id.btnCos);
        btnRadians = (Button)findViewById(R.id.btnRadians);
        btnDegrees = (Button)findViewById(R.id.btnDegree);
        btnCalculator = (Button)findViewById(R.id.btnCalculator);

        firstNum = (EditText)findViewById(R.id.etFirst);
        secondNum = (EditText)findViewById(R.id.etSecond);

        tvResultF = (TextView)findViewById(R.id.tvResultF);
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case(R.id.btnCalculator):
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case(R.id.btnSquare):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.pow(secNum, 2.00);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnCube):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.pow(secNum, 3.00);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnRaiseTo):
                try{
                    firNum = Double.parseDouble(firstNum.getText().toString());
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.pow(secNum, firNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnSquareRoot):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.sqrt(secNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnMax):
                try{
                    firNum = Double.parseDouble(firstNum.getText().toString());
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.max(secNum, firNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnMin):
                try{
                    firNum = Double.parseDouble(firstNum.getText().toString());
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.min(secNum, firNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnSin):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.sin(secNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnTan):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.tan(secNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnRound):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.round(secNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnHyp):
                try{
                    firNum = Double.parseDouble(firstNum.getText().toString());
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.sqrt((firNum*firNum) + (secNum*secNum));
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnFloor):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.floor(secNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnCos):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.cos(secNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnRadians):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.toRadians(secNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;
            case(R.id.btnDegree):
                try{
                    secNum = Double.parseDouble(secondNum.getText().toString());
                    result = Math.toDegrees(secNum);
                    tvResultF.setText(String.format("%.3f",(float)result));
                }catch(Exception e){

                }
                break;

        }
    }
}
