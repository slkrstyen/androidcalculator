package com.example.scott.simplecalc14;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnAdd, btnMinus, btnMultiply, btnDivide, btnEquals, btnClear, btnFormulas;
    private TextView etResult;
    private int firstNum, secondNum, result;
    private String operator;
    private StringBuilder sb = new StringBuilder();
    private boolean first = false;
    private boolean operatorSym = false;
    private boolean second = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        init();
    }

    private void init() {
        btn0 = (Button)findViewById(R.id.btn0);
        btn1 = (Button)findViewById(R.id.btn1);
        btn2 = (Button)findViewById(R.id.btn2);
        btn3 = (Button)findViewById(R.id.btn3);
        btn4 = (Button)findViewById(R.id.btn4);
        btn5 = (Button)findViewById(R.id.btn5);
        btn6 = (Button)findViewById(R.id.btn6);
        btn7 = (Button)findViewById(R.id.btn7);
        btn8 = (Button)findViewById(R.id.btn8);
        btn9 = (Button)findViewById(R.id.btn9);
        btnAdd = (Button)findViewById(R.id.btnAdd);
        btnMinus = (Button)findViewById(R.id.btnMinus);
        btnMultiply = (Button)findViewById(R.id.btnMultiply);
        btnDivide = (Button)findViewById(R.id.btnDivide);
        btnEquals = (Button)findViewById(R.id.btnEquals);
        btnClear = (Button)findViewById(R.id.btnClear);
        etResult = (TextView) findViewById(R.id.etResult);
        btnFormulas = (Button)findViewById(R.id.btnFormulas);


        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnMultiply.setOnClickListener(this);
        btnDivide.setOnClickListener(this);
        btnEquals.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnFormulas.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case(R.id.btn0):
                sb.append(0);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btn1):
                sb.append(1);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btn2):
                sb.append(2);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btn3):
                sb.append(3);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btn4):
                sb.append(4);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btn5):
                sb.append(5);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btn6):
                sb.append(6);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btn7):
                sb.append(7);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btn8):
                sb.append(8);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btn9):
                sb.append(9);
                etResult.setText(sb);
                if(first == false){
                    first = true;
                }
                break;
            case(R.id.btnAdd):
                if(first == true) {
                    try {
                        firstNum = Integer.parseInt(etResult.getText().toString());
                        etResult.setText(null);
                        operator = "+";
                        sb.setLength(0);
                        operatorSym = true;
                    }catch (Exception e){

                    }
                }
                break;
            case(R.id.btnMinus):
                try {
                    firstNum = Integer.parseInt(etResult.getText().toString());
                    etResult.setText(null);
                    operator = "-";
                    sb.setLength(0);
                    operatorSym = true;
                }catch (Exception e){

                }
                break;
            case(R.id.btnMultiply):
                try {
                    firstNum = Integer.parseInt(etResult.getText().toString());
                    etResult.setText(null);
                    operator = "*";
                    sb.setLength(0);
                    operatorSym = true;
                }catch (Exception e){

                }
                break;
            case(R.id.btnDivide):
                try {
                    firstNum = Integer.parseInt(etResult.getText().toString());
                    etResult.setText(null);
                    operator = "/";
                    sb.setLength(0);
                    operatorSym = true;
                }catch (Exception e){

                }

                break;
            case(R.id.btnEquals):
                if(first == true && operatorSym == true) {
                    try {
                        secondNum = Integer.parseInt(etResult.getText().toString());
                        if (operator == "+") {
                            result = firstNum + secondNum;
                        } else if (operator == "-") {
                            result = firstNum - secondNum;
                        } else if (operator == "*") {
                            result = firstNum * secondNum;
                        } else if (operator == "/") {
                            try {
                                result = firstNum / secondNum;
                            } catch (Exception e) {
                                etResult.setText("Cannot Divide by Zero");
                            }
                        }
                        etResult.setText(String.valueOf(result));

                        sb.setLength(0);
                    }catch (Exception e){

                    }
                }

                break;
            case(R.id.btnClear):
                sb.setLength(0);
                etResult.setText("");
                first = false;
                second = false;
                operatorSym = false;
                break;
            case(R.id.btnFormulas):
                Intent intent = new Intent(this, Formulas.class);
                startActivity(intent);
                finish();
                break;

        }
    }
}
